obj-m += led_display.o

module-objs := led_display_gpio.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) clean
lazy_push:
	git commit -a -m "Lazy Push"
	git push
