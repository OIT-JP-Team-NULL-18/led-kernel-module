/* led_display.c
 *
 * Copyright 2018 Team NULL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#include "led_display_gpio.h"
#include "led_display_thread.h"

#define DEVICE_NAME         "led_display"
#define CLASS_NAME          "led_display"
#define DRIVER_VERSION      "0.1"
#define BUFFER_SIZE 10
/*Module information*/
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Team NULL");
MODULE_DESCRIPTION("LED Display Software Module for RPI");
MODULE_VERSION(DRIVER_VERSION);

/* Prototypes for device functions */
static int device_open(struct inode *,
                       struct file *);
static int device_release(struct inode *,
                          struct file *);
static ssize_t device_read(struct file *,
                           char *,
                           size_t,
                           loff_t *);
static ssize_t device_write(struct file *,
                            const char *,
                            size_t,
                            loff_t *);

/*Device Data*/
int led_display_major_number = -1;
char led_display_buffer[BUFFER_SIZE];
char * led_display_ptr = NULL;
int led_display_open = 0;


/* This structure points to all of the device functions */
static struct file_operations file_ops = {
 .read = device_read,
 .write = device_write,
 .open = device_open,
 .release = device_release
};

static ssize_t device_read(struct file *flip,
                           char *buffer,
                           size_t len,
                           loff_t *offset)
{
    printk(KERN_ALERT "led_display: this is a write only module");
    return 0;
}
static ssize_t device_write(struct file *flip,
                            const char *buffer,
                            size_t len,
                            loff_t * offset)
{

    strncpy(led_display_buffer, buffer, len);
    printk(KERN_INFO "led_display: received: %s", led_display_buffer);
    return 0;
}
static int device_release(struct inode *inode,
                          struct file *file) {
    led_display_open--;
    module_put(THIS_MODULE);
    return 0;
}

/* Called when a process opens our device */
static int device_open(struct inode *inode,
                       struct file *file) {
    /* If device is open, return busy */
    if (led_display_open) {
    return -EBUSY;
    }
    led_display_open++;
    try_module_get(THIS_MODULE);
    return 0;
}

static int __init led_display_init (void)
{
    printk(KERN_INFO "led_display: initializing");
    led_display_major_number = register_chrdev(0, "led_display", &file_ops);
    if (led_display_major_number < 0)
    {
        printk(KERN_ALERT "led_display: couldn't register the device %d\n", led_display_major_number);
    }
    else
    {
        printk(KERN_INFO "led_display: successfully registered the device\n");
        return 0;
    }
    return 0;
}
static void __exit led_display_exit (void)
{
    unregister_chrdev(led_display_major_number, "led_display");

}
/*Define init function*/
module_init(led_display_init);
/*Define exit function*/
module_exit(led_display_exit);

