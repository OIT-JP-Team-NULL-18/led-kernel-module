#include "led_display_gpio.h"

#define LED_PIN_R1                  21
#define LED_PIN_R2                  22
#define LED_PIN_OE                  5
#define LED_PIN_CLK                 4
//LE
#define LED_PIN_LAT                 3
//A1
#define LED_PIN_A                   0
//A2
#define LED_PIN_B                   1
//A3
#define LED_PIN_C                   2

void led_display_init_gpio(void){
    printk(KERN_INFO "led_display: starting gpio");
    gpio_request(LED_PIN_R1, "R1");
    gpio_request(LED_PIN_R2, "R2");
    gpio_request(LED_PIN_A, "A");
    gpio_request(LED_PIN_B, "B");
    gpio_request(LED_PIN_C, "C");

    gpio_request(LED_PIN_OE, "OE");
    gpio_request(LED_PIN_CLK, "CLK");
    gpio_request(LED_PIN_LAT, "LAT");

    gpio_direction_output(LED_PIN_R1, 0);
    gpio_direction_output(LED_PIN_R2, 0);

    gpio_direction_output(LED_PIN_A, 0);
    gpio_direction_output(LED_PIN_B, 0);
    gpio_direction_output(LED_PIN_C, 0);

    gpio_direction_output(LED_PIN_OE, 1);
    gpio_direction_output(LED_PIN_LAT, 0);
    gpio_direction_output(LED_PIN_CLK, 0);
    printk(KERN_IFNO "led_display: started gpio")
}
void led_display_gpio_exit(void){
    printk(KERN_INFO "led_display: stopping gpio");
    gpio_free(LED_PIN_R1);
    gpio_free(LED_PIN_R2);

    gpio_free(LED_PIN_OE);
    gpio_free(LED_PIN_CLK);
    gpio_free(LED_PIN_LAT);

    gpio_free(LED_PIN_A);
    gpio_free(LED_PIN_B);
    gpio_free(LED_PIN_C);
    printk(KERN_INFO "led_display: stopped gpio");

}
