#include "led_display_thread.h"

#define THREAD_NAME "led_display"
int led_display_thread(void * data){
    uint8_t line;
    uint8_t pos;
    uint8_t bit;
    struct task_struct * TSK;
    struct sched_param PARAM = { .sched_priority = MAX_RT_PRIO };
    TSK = current
    PARAM.sched_priority = THREAD_PRIORITY;
    sched_setscheduler(TSK, SCHED_FIFO, &PARAM);

    while(1){
        usleep_range(2000, 2000);
        if (kthread_should_stop())
            break;
    }
    return 0;
}

void led_display_thread_init(void){
    printk(KERN_INFO "led_display: starting thread");
    task = kthread_run(pix_thread, NULL, THREAD_NAME);
    printk(KERN_INFO "led_display: starting thread done");
}

void led_display_thread_exit(void){
    printk(KERN_INFO "led_display: stopping thread");
    kthread_stop(task);
    printk(KERN_INFO "led_display: thread stopped");
}
