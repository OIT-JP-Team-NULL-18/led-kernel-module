#ifndef _LED_DISPLAY_GPIO_H
#define _LED_DISPLAY_GPIO_H

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/gpio.h>

void led_display_init_gpio(void);
void led_display_gpio_exit(void);
#endif
