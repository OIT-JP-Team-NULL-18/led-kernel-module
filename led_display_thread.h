#ifndef _LED_DISPLAY_THREAD_H
#define _LED_DISPLAY_THREAD_H
#include <linux/kthread.h>
#include <linux/delay.h>

int led_display_thread(void * data);
void led_display_thread_init(void);
void led_display_thread_exit(void);
#endif
